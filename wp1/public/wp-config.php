<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp1');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R-1Ys[wsew!=q0|M[HFY`H3<U[T#>brycE9|&xv#|#b1[$xh)d*`_#{4cICfK4k,');
define('SECURE_AUTH_KEY',  'NWVY-P7ZH%I@&N-(wQ(]@rD6ujxgTm$n_J[Dl!F#,Mnb{X$Y7-QsxHgdXk|Lg*Xr');
define('LOGGED_IN_KEY',    'SA*%%7Yv|pf`6R9skm-R~JbJS [sDiClz!%?T}43R9m1JzfZ*LXaFU&$6a>{!V]-');
define('NONCE_KEY',        'W;43F]#$06H~8Jn<Nuda>H_-;[n(D3J*<i;S8`*(.L[h=+f4#]7EcZq|ypj[/Q*o');
define('AUTH_SALT',        'HyX:2&Vk|_:vvnn(Ah~n/r3tcxfq!cR6Oy)@;%W2gzF S4#S]%4IK0;9h(ep=psg');
define('SECURE_AUTH_SALT', 'Fp;7=POE[; MWgct]uJvubp#CWybs+!iDGb.tonW6ehpvfYM==}KU7O^{rDG3O;[');
define('LOGGED_IN_SALT',   ')B_e]bpu>v{IdNJ^vYmQRBQx6I;0jaXt_m(qx&oG9J1X;RVzTpW{!i>oj))tFg<1');
define('NONCE_SALT',       'FHit)AH{y8d;G:BPZ?~^y:[k85[Kwey5 81cW-i&vsR=kGP+j%Zp,REdRg]B#D](');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wdd';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
