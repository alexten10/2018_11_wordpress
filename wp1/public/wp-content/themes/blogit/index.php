<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

  <title><?php echo get_bloginfo('name'); ?></title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  
  <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />
  <?php wp_head(); ?>
</head>
<body>
  <div class="container">

    <header class="main">

      <span class="site_title"><?php echo get_bloginfo('name'); ?></span>

    </header>

    <div id="content">


      <div id="primary">
        
        <?php if(is_archive()) : ?><!-- if page archive is displayed, display month and year -->
          <h1 class='archive_title'><?php echo get_the_archive_title(); ?></h1>
        <?php endif; ?>
        
        <!--Determines if the query is for the blog homepage.-->
        <?php if ( is_home() ) : ?>
          <h1>Our Blog</h1>
        <?php endif; ?>
        
        
        <?php while(have_posts()) : the_post() ?><!--while loop + autoincrement -->
        
        
          <article>
            
            <!-- for single pages or posts ahow h1, else ahow h2with link-->
            <?php if( !is_single() && !is_page() ) : ?>
              <h2><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h2><!-- same <?php //the_title('<h1>', '</h1>'); ?> -->
            <?php else : ?>
              <h1><?php the_title(); ?></h1>
            <?php endif; ?>

            <?php the_content('<p>', '</p>'); ?>

          </article><!-- /article -->
        
        
        <?php endwhile; ?>
        
      </div><!-- /primary -->




      <div id="secondary">

        <h3>Menu</h3>
        <?php wp_nav_menu(['menu_class' => 'menu', 'container' => 'ul']); ?>

        <h3>Archive</h3>
        <ul class="menu"><?php wp_get_archives(); ?></ul>

      </div><!-- /secondary -->




    </div><!-- /content -->

    <footer class="main">

        <p class="copyright">Copyright &copy; 2015 by Blogit</p>
    </footer>
    
  </div><!-- /container -->
  <?php wp_footer(); ?>
  </body>
</html>