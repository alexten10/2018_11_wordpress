<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>My Blog</title>
  
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" /><!-- look for css in theme folder, no need to specify path -->
  
  <?php wp_head(); ?>
</head>



<body>
  
  <div class="container">
    
    <div class="main">
      
      
      <?php while(have_posts()) : ?>
        <?php the_post(); ?> <!-- $post() is an incrementer -->
        
        <!--
        <pre>
          <?php //var_dump($post); ?>
        </pre>
        -->
        
        <?php the_title('<h1>', '</h1>'); ?> <!--display title, 1 - before, 2 -after -->
        
        <p><small>Posted on <?php the_date(); ?> at <?php the_time(); ?> by <?php the_author(); ?></small></p>
        
        <?php the_content('<div>', '</div>'); ?>
        
        <!--  dont get link on a single post page  -->
        <?php if(!is_single()) : ?>
          <a href="<?php the_permalink(); ?>" >Read more...</a><!-- link for current post, takes only one parameter-->
        <?php endif; ?>
        
      <?php endwhile; ?>
      
      
    </div><!-- end main-->
    
    
    
    <div class="sidebar">
      
      <h2>sidebar</h2>
      
      <h3>menu</h3>
      <?php wp_nav_menu(); ?> <!-- get menu  -->
      
      <h3>search</h3>
      <form action="/" method="get">
        <input type="text" name="s" />
        <input type="submit" value="search" />
      </form>
      
      <h3>archives</h3>
      <?php wp_get_archives(); ?> <!-- get archives by month -->
      
      <h3>categories</h3>
      <?php wp_list_categories(['title_li' => '']); ?> <!-- get categories -->
      
      
      
      
    </div><!--end sidebar-->
    
    
    
  </div><!--end container -->
  
  
  <?php wp_footer(); ?>
</body>
</html>








