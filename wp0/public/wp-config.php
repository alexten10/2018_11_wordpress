<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp0');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kanRWD_slHIl8kS01,q6y@%qCt<AD@FeomvFcdS~9Gp}!7g]%fl?0<+noN-P5O-:');
define('SECURE_AUTH_KEY',  't43loepnNZlP9qg;)78hmuCu{d$Qvcw. 8p+Ji0G6}t.G)GF8X}1uwN{d>,a;[];');
define('LOGGED_IN_KEY',    'L:3:bnqa}--^=T^`Mb2rE;SRmuM:64y{,:1^@zI`Aw=6?LRwLXQR^_(PH?;# J X');
define('NONCE_KEY',        'vpP7OW 0vL>W#D}l2}eg]~vW|G#5Mlke.y]ASc~p^.5>5/q{Y{,~THyr<=|RaaU,');
define('AUTH_SALT',        'V(T>^(`}3cTARJb2Mvp)/cl-fmx597Idt:;~(Iu9I$EWJB&$+?pLhA w{D_^n~8<');
define('SECURE_AUTH_SALT', '6&7tSLo*/:Ct1a<Mm#AG&E>Eu3${@Y CD[ :m]@+}#NTm@f2j,hE/1=YmeyxUL!^');
define('LOGGED_IN_SALT',   'AFW`ch+|yJP^p#/(9Vo=!Cd$sK1Mq8]55|sM~LWzr:/iDYxL]{x%5W[[/!IaVp?o');
define('NONCE_SALT',       't/5`HXK_E/l#vTq-:CZFt*D{aeUq!O*9o]^pla^8NUXg_W~;@.nRvYL(Zh+bI/e!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wdd';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
